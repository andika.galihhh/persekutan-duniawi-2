from django.shortcuts import render
from .forms import MatkulForm
from .models import MatkulModel
from django.http import HttpResponseRedirect

def detail(request,detail_id):
    course_detail = MatkulModel.objects.get(id=detail_id)

    context = {
        'course_detail':course_detail,
    }
    return render(request, 'detail.html', context)

def delete(request,id):
    MatkulModel.objects.filter(id=id).delete()
    return HttpResponseRedirect('/matkul')

def update(request,id):
    matkul_update = MatkulModel.objects.get(id=id)

    data = {
        'nama_mata_kuliah' : matkul_update.nama_mata_kuliah,
        'dosen_pengajar' : matkul_update.dosen_pengajar,
        'jumlah_sks' : matkul_update.jumlah_sks,
        'ruangan' : matkul_update.ruangan,
        'deskripsi_mata_kuliah' : matkul_update.deskripsi_mata_kuliah,
        'semester_tahun' : matkul_update.semester_tahun,
    }
    matkul_form = MatkulForm(request.POST or None, initial=data, instance=matkul_update)
    
    context = {
        'headin' : 'Matkul Form',
        'form' : matkul_form
    }
    if request.method == 'POST':

        if matkul_form.is_valid():
            matkul_form.save()
            return HttpResponseRedirect('/matkul')


    return render(request,'form.html',context)

def matkul(request):
    posts = MatkulModel.objects.all()

    context = {
        'page_title':'list_post',
        'posts':posts,

    }
    return render(request,'matkul.html',context)
    
def form(request):
    matkul_form = MatkulForm(request.POST or None)

    context = {
        'headin' : 'Matkul Form',
        'form' : matkul_form
    }
    
    if request.method == 'POST':

        if matkul_form.is_valid():
            matkul_form.save()
            # MatkulModel.objects.create(
            #     nama_mata_kuliah = matkul_form.cleaned_data.get('nama_mata_kuliah'),
            #     dosen_pengajar = matkul_form.cleaned_data.get('dosen_pengajar'),
            #     jumlah_sks = matkul_form.cleaned_data.get('jumlah_sks'),
            #     deskripsi_mata_kuliah = matkul_form.cleaned_data.get('deskripsi_mata_kuliah'),
            #     semester_tahun = matkul_form.cleaned_data.get('semester_tahun'),

            # )
            return HttpResponseRedirect('matkul')

    return render(request,'form.html',context)


def index(request):
    return render(request,"index.html")

# def home(request):
#     return render(request,"index.html")

# def story1(request):
#     return render(request,"story1.html")

def project(request):
    return render(request,"index2.html")

