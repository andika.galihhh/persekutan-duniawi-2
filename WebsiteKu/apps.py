from django.apps import AppConfig


class WebsitekuConfig(AppConfig):
    name = 'WebsiteKu'
