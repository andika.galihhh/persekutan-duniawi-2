from django import forms
from .models import MatkulModel
from django.forms import TextInput,Select,Textarea

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatkulModel
        fields = [
            'nama_mata_kuliah',
            'dosen_pengajar',
            'jumlah_sks',
            'ruangan',
            'deskripsi_mata_kuliah',
            'semester_tahun',
        ]

        widgets = {
                'nama_mata_kuliah': forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder': 'ex. Matematika Dasar'
                    }
                ),
                'dosen_pengajar': forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder': 'Nama dosen Pengajar'
                    }
                ),
                'jumlah_sks': forms.Select(
                    attrs={
                        'class':'form-control col-lg-2',
                    }
                ),
                'ruangan': forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder': 'ex. 2.2522'
                    }
                ),
                'deskripsi_mata_kuliah': forms.Textarea(
                    attrs={
                        'class':'form-control',
                        'placeholder': 'Tuliskan Deskripsi'
                    }
                ),
                'semester_tahun': forms.Select(
                    attrs={
                        'class':'form-control col-lg-2',
                    }
                ),

        }

      
    # nama_mata_kuliah = forms.CharField(
    #     max_length=20,
    #     label = 'Nama Mata Kuliah',
    #     widget = forms.TextInput(
    #             attrs={
    #                 'class':'form-control',
    #             }
    #         )
    #     )

    # dosen_pengajar = forms.CharField(
       
    #     label = 'Dosen Pengajar',
    #     widget = forms.TextInput(
    #         attrs={
    #             'class':'form-control',
    #         }
    #     )
    # )
    # SKS = (
    #     ('1','1'),
    #     ('2','2'),
    #     ('3','3'),
    #     ('4','4'),
    #     ('5','5'),
    #     ('6','6'),
    # )

    # jumlah_sks = forms.ChoiceField(choices=SKS)

    # deskrpsi_mata_kuliah = forms.CharField(
    #     max_length=120,
    #     label = 'Dosen Pengajar',
    #     widget = forms.Textarea(
    #          attrs={
    #             'class':'form-control',
    #         }
    #     )
    # )
    # SEMESTER =(
    #     ('Ganjil 2019/2020','Ganjil 2019/2020'),
    #     ('Gasal 2019/2020','Gasal 2019/2020'),
    #     ('Ganjil 2020/2021','Ganjil 2020/2021'),
    #     ('Gasal 2020/2021','Gasal 2020/2021')
    # )

    # semester_tahun = forms.ChoiceField(choices=SEMESTER)

