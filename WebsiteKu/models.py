from django.db import models
from django import forms
# Create your models here.

class MatkulModel(models.Model):
    nama_mata_kuliah = models.CharField(max_length = 50)
    dosen_pengajar = models.CharField(max_length = 50)
    jumlah_sks = models.CharField(
        max_length = 2,
        choices = [
            ('1','1'),
            ('2','2'),
            ('3','3'),
            ('4','4'),
            ('5','5'),
            ('6','6'),
        ],
        default = 1
    )
    ruangan = models.CharField(max_length = 50)
    
    deskripsi_mata_kuliah = models.TextField()

    semester_tahun = models.CharField(
        max_length = 20,
        choices = [
            ('Ganjil 2019/2020','Ganjil 2019/2020'),
            ('Gasal 2019/2020','Gasal 2019/2020'),
            ('Ganjil 2020/2021','Ganjil 2020/2021'),
            ('Gasal 2020/2021','Gasal 2020/2021'),
        ],
        default = 'Ganjil 2019/2020'                                   
    )

    def __str__(self):
        return "{}. {}".format(self.id,self.nama_mata_kuliah)
