from django.urls import path
from . import views

app_name = 'WebsiteKu'

urlpatterns = [
    path('detail/<int:detail_id>', views.detail, name='detail'),
    path('delete/<int:id>', views.delete, name = 'delete'),
    path('update/<int:id>', views.update, name = 'update'),
    path('',views.index, name='index'),
    path('project',views.project, name= 'project'),
    path('form',views.form, name='form'),
    path('matkul',views.matkul, name='matkul')

]